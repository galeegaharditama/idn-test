package idn.test.sudokusolver.view

data class Cell(
    val row: Int,
    val col: Int,
    var value: Int?=null,
    var isStartingCell: Boolean = false,
)
