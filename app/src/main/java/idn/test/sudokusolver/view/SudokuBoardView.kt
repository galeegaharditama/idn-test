package idn.test.sudokusolver.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.ColorInt
import idn.test.sudokusolver.R
import idn.test.sudokusolver.helper.getColorCompat

class SudokuBoardView(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {

    private var listener: OnTouchListener? = null

    private var sqrtSize = 3
    private var size = 9

    private var cellSizePixels = 0F

    private var selectedRow = 0
    private var selectedCol = 0

    private var cells: List<Cell>? = null

    private var colorLight: Int = R.color.blue_grey_500
    private var colorDark: Int = R.color.blue_grey_700

    fun setThickLinePaint(paint: Paint) {
        thickLinePaint = paint
    }

    fun setColorLight(@ColorInt color: Int) {
        colorLight = color
    }

    fun setColorDark(@ColorInt color: Int) {
        colorDark = color
    }

    private var thickLinePaint = Paint().apply {
        style = Paint.Style.STROKE
        color = context.getColorCompat(R.color.yellow_800)
        strokeWidth = 4F
    }

    private val selectedCellPaint = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        color = context.getColorCompat(R.color.yellow_800)
    }

    private var textPaint = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        color = Color.WHITE
        textSize = 24F
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val sizePixels = Math.min(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(sizePixels, sizePixels)
    }

    override fun onDraw(canvas: Canvas) {
        cellSizePixels = (width / size).toFloat()
        colorCells(canvas)
        fillCells(canvas)
        drawLines(canvas)
        drawText(canvas)
    }

    private fun colorCells(canvas: Canvas) {
        for (r in 0..size) {
            for (c in 0..size) {
                if (r % 2 == 0) {
                    if (c % 2 == 0) fillCell(canvas, r, c, Paint().apply {
                        style = Paint.Style.FILL_AND_STROKE
                        color = context.getColorCompat(colorLight)
                    })
                    else fillCell(canvas, r, c, Paint().apply {
                        style = Paint.Style.FILL_AND_STROKE
                        color = context.getColorCompat(colorDark)
                    })
                } else {
                    if (c % 2 == 0) fillCell(canvas, r, c, Paint().apply {
                        style = Paint.Style.FILL_AND_STROKE
                        color = context.getColorCompat(colorDark)
                    })
                    else fillCell(canvas, r, c, Paint().apply {
                        style = Paint.Style.FILL_AND_STROKE
                        color = context.getColorCompat(colorLight)
                    })
                }
            }
        }
    }

    private fun fillCells(canvas: Canvas) {
        if (selectedRow == -1 || selectedCol == -1) return

        for (r in 0..size) {
            for (c in 0..size) {
                if (r == selectedRow && c == selectedCol) {
                    fillCell(canvas, r, c, selectedCellPaint)
                }
            }
        }
    }

    private fun fillCell(canvas: Canvas, r: Int, c: Int, paint: Paint) {
        canvas.drawRect(
            c * cellSizePixels,
            r * cellSizePixels,
            (c + 1) * cellSizePixels,
            (r + 1) * cellSizePixels,
            paint
        )
    }

    private fun drawLines(canvas: Canvas) {
        canvas.drawRect(0F, 0F, width.toFloat(), height.toFloat(), thickLinePaint)

        for (i in 1 until size) {
            when (i % sqrtSize) {
                0 -> {
                    canvas.drawLine(
                        i * cellSizePixels,
                        0F,
                        i * cellSizePixels,
                        height.toFloat(),
                        thickLinePaint
                    )

                    canvas.drawLine(
                        0F,
                        i * cellSizePixels,
                        width.toFloat(),
                        i * cellSizePixels,
                        thickLinePaint
                    )
                }
            }
        }
    }

    private fun drawText(canvas: Canvas) {
        cells?.forEach {
            val row = it.row
            val col = it.col
            if (it.value != null){
                val valueString = it.value.toString()

                val textBounds = Rect()
                textPaint.getTextBounds(valueString, 0, valueString.length, textBounds)
                val textWidth = textPaint.measureText(valueString)
                val textHeight = textBounds.height()

                canvas.drawText(valueString, (col * cellSizePixels) + cellSizePixels / 2 - textWidth / 2,
                    (row * cellSizePixels) + cellSizePixels / 2 - textHeight / 2, textPaint)
            } else {
                val textBounds = Rect()
                textPaint.getTextBounds("", 0, 0, textBounds)
                val textWidth = textPaint.measureText("")
                val textHeight = textBounds.height()

                canvas.drawText("", (col * cellSizePixels) + cellSizePixels / 2 - textWidth / 2,
                    (row * cellSizePixels) + cellSizePixels / 2 - textHeight / 2, textPaint)
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                handleTouchEvent(event.x, event.y)
                true
            }
            else -> false
        }
    }

    private fun handleTouchEvent(x: Float, y: Float) {
        val possibleSelectedRow = (y / cellSizePixels).toInt()
        val possibleSelectedCol = (x / cellSizePixels).toInt()
        listener?.onCellTouched(possibleSelectedRow, possibleSelectedCol)
    }

    fun updateSelectedCellUI(row: Int, col: Int) {
        selectedRow = row
        selectedCol = col
        invalidate()
    }

    fun updateCells(cells: List<Cell>) {
        this.cells = cells
        invalidate()
    }

    fun registerListener(listener: OnTouchListener) {
        this.listener = listener
    }

    interface OnTouchListener {
        fun onCellTouched(row: Int, col: Int)
    }
}