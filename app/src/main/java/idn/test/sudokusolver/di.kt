package idn.test.sudokusolver

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModels = module {
    viewModel { MainVM() }
}

val mainModule = listOf(
    viewModels
)