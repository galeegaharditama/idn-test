package idn.test.sudokusolver

import idn.test.sudokusolver.view.Board

class SudokuSolver(pBoard: Board) {

    private var board: Board = pBoard

    private fun chekInRow(row: Int, value: Int): Boolean {
        for (i in 0 until this.board.size) {
            if (board.getCell(row, i).value == value)
                return true
        }
        return false
    }

    private fun chekInColumn(column: Int, value: Int): Boolean {
        for (i in 0 until this.board.size) {
            if (board.getCell(i, column).value == value) {
                return true
            }
        }
        return false
    }

    private fun checkInBox(row: Int, column: Int, value: Int): Boolean {
        val r = row - row % 3
        val c = column - column % 3

        for (i in r until r + 3) {
            for (j in c until c + 3) {
                if (board.getCell(i, j).value == value) return true
            }
        }
        return false
    }

    private fun checkAll(row: Int, column: Int, value: Int): Boolean {
        return !chekInRow(row, value) && !chekInColumn(column, value) && !checkInBox(row, column, value)
    }

    fun getSolve(): Boolean {
        for (row in 0 until board.size) {
            for (column in 0 until board.size) {
                if (board.getCell(row, column).value == null) {
                    for (number in 1..9) {
//                        val test = checkAll(row, column, number)
//                        println("$row $column $number $test")
                        if (checkAll(row, column, number)) {
                            board.getCell(row, column).value = number

                            if (getSolve()) return true
                            else {
                                board.getCell(row, column).value = null
                            }
                        }
                    }
                    return false
                }
            }
        }
        return true
    }

    fun getBoard() = board
}