package idn.test.sudokusolver

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import idn.test.sudokusolver.view.Board
import idn.test.sudokusolver.view.Cell
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class MainVM : ViewModel() {
    private var _showError = MutableLiveData<String>()
    val showError: LiveData<String> = _showError

    var selectedCellLiveData = MutableLiveData<Pair<Int, Int>>()
    var cellsLiveData = MutableLiveData<List<Cell>>()

    private var selectedRow = -1
    private var selectedCol = -1

    var secondsLiveData = MutableLiveData(0)
    var timeRunning = MutableLiveData(false)
    var isRunning = MutableLiveData(false)

    private val board: Board

    init {
        val cells = List(9 * 9) {i -> Cell(i / 9, i % 9, null)}
        board = Board(9, cells)

        selectedCellLiveData.postValue(Pair(selectedRow, selectedCol))
        cellsLiveData.postValue(board.cells)
        timeRunning.postValue(true)
    }

    fun solveSudoku(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val solver = SudokuSolver(board)
                if (solver.getSolve()) {
                    cellsLiveData.postValue(solver.getBoard().cells)
                    timeRunning.postValue(false)
                }
                else throw Throwable("Sudoku Tidak Dapat Diselesaikan")
            } catch (e:Throwable){
                _showError.postValue(e.localizedMessage)
            }
        }
    }

    fun handleInput(number: Int) {
        if (selectedRow == -1 || selectedCol == -1) return
        val cell = board.getCell(selectedRow, selectedCol)
        if (cell.isStartingCell) return

        cell.value = number
        cellsLiveData.postValue(board.cells)
    }


    fun updateSelectedCell(row: Int, col: Int) {
        val cell = board.getCell(row, col)
        if (!cell.isStartingCell) {
            selectedRow = row
            selectedCol = col
            selectedCellLiveData.postValue(Pair(row, col))
        }
    }

    fun reset(){
        board.cells.forEach {
            it.value = null
        }
        cellsLiveData.postValue(board.cells)
        secondsLiveData.postValue(0)
        timeRunning.postValue(false)
        timeRunning.postValue(true)
    }

    fun delete() {
        val cell = board.getCell(selectedRow, selectedCol)
        cell.value = null
        cellsLiveData.postValue(board.cells)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}