package idn.test.sudokusolver.helper

import android.content.Context
import androidx.core.content.ContextCompat

fun Context.getColorCompat(color: Int) = ContextCompat.getColor(this, color)
