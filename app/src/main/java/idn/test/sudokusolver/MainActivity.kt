package idn.test.sudokusolver

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import idn.test.sudokusolver.databinding.ActivityMainBinding
import idn.test.sudokusolver.view.Cell
import idn.test.sudokusolver.view.SudokuBoardView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), SudokuBoardView.OnTouchListener {

    private lateinit var binding: ActivityMainBinding
    private val mainVM: MainVM by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btn1.root.text = "1"
        binding.btn2.root.text = "2"
        binding.btn3.root.text = "3"
        binding.btn4.root.text = "4"
        binding.btn5.root.text = "5"
        binding.btn6.root.text = "6"
        binding.btn7.root.text = "7"
        binding.btn8.root.text = "8"
        binding.btn9.root.text = "9"

        binding.btn1.root.setOnClickListener {
            mainVM.handleInput("${binding.btn1.root.text}".toIntOrNull() ?: 0)
        }
        binding.btn2.root.setOnClickListener {
            mainVM.handleInput("${binding.btn2.root.text}".toIntOrNull() ?: 0)
        }
        binding.btn3.root.setOnClickListener {
            mainVM.handleInput("${binding.btn3.root.text}".toIntOrNull() ?: 0)
        }
        binding.btn4.root.setOnClickListener {
            mainVM.handleInput("${binding.btn4.root.text}".toIntOrNull() ?: 0)
        }
        binding.btn5.root.setOnClickListener {
            mainVM.handleInput("${binding.btn5.root.text}".toIntOrNull() ?: 0)
        }
        binding.btn6.root.setOnClickListener {
            mainVM.handleInput("${binding.btn6.root.text}".toIntOrNull() ?: 0)
        }
        binding.btn7.root.setOnClickListener {
            mainVM.handleInput("${binding.btn7.root.text}".toIntOrNull() ?: 0)
        }
        binding.btn8.root.setOnClickListener {
            mainVM.handleInput("${binding.btn8.root.text}".toIntOrNull() ?: 0)
        }
        binding.btn9.root.setOnClickListener {
            mainVM.handleInput("${binding.btn9.root.text}".toIntOrNull() ?: 0)
        }

        binding.btnSolve.setOnClickListener {
            mainVM.solveSudoku()
        }

        binding.btnNewGame.setOnClickListener {
            mainVM.reset()
        }

        binding.sudokuBoard.registerListener(this)

        mainVM.selectedCellLiveData.observe(this, { updateSelectedCellUI(it) })
        mainVM.cellsLiveData.observe(this, { updateCells(it) })
        mainVM.secondsLiveData.observe(this, { updateTimerUI(it) })
        mainVM.showError.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        val handler = Handler(Looper.getMainLooper())
        handler.post(object: Runnable {
            override fun run() {
                if (mainVM.timeRunning.value == true) {
                    mainVM.secondsLiveData.value = mainVM.secondsLiveData.value?.plus(1)
                }
                handler.postDelayed(this, 1000)
            }
        })
    }



    private var stop = false

    private fun updateCells(cells: List<Cell>?) = cells?.let {
        binding.sudokuBoard.updateCells(cells)
    }

    private fun updateSelectedCellUI(cell: Pair<Int, Int>?) = cell?.let {
        binding.sudokuBoard.updateSelectedCellUI(cell.first, cell.second)
    }

    private fun updateTimerUI(timer:Int?){
        timer?.let {
            val seconds = it
            val hours = seconds / 3600
            val minutes = (seconds % 3600) / 60
            val secs = seconds % 60

            val times = "%d:%02d:%02d".format(hours, minutes, secs)
            binding.timeRemain.text = times
        }
    }

    override fun onCellTouched(row: Int, col: Int) {
        mainVM.updateSelectedCell(row, col)
    }
}